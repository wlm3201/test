// ==UserScript==
// @name         Auto Show Answer
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  csu
// @author       wlm3201
// @match        http://szgxjx.csu.edu.cn/zndxstudent/*
// @icon         https://www.google.com/s2/favicons?domain=csu.edu.cn
// @grant        none
// ==/UserScript==

(function() {
    var i = 0
    setTimeout(function() {
        var sa = document.getElementsByClassName('showBtn')
        for (i = 0; i < sa.length; i++) {
            sa[i].click();
        }
        if (typeof(document.getElementsByClassName('question-num select-exam-number')[0]) == "undefined") {
            var asws = document.getElementsByClassName('answerExam')
            var asw = new Array();
            var opt = document.getElementsByClassName('dar_optext')
            for (i = 0; i < asws.length; i++) {
                asw[i] = asws[i].innerText[5]
            }
            document.getElementsByClassName('question-num')[41].click()
            for (i = 0; i < asw.length; i++) {
                if (typeof(asw[i]) != "undefined") {
                    opt[i].click()
                }
            }
            document.getElementsByClassName('question-num')[0].click()
            for (i = 0; i < 108; i++) {
                document.getElementsByClassName('ant-btn ant-btn-default')[1].click()
            }
        }
        document.getElementsByClassName('subjectBox')[0].addEventListener('click', function() {
            document.getElementsByClassName('ant-btn ant-btn-default')[1].click()
        }, false)
        document.getElementsByClassName('subjectBox')[0].oncontextmenu = function(e) {
            e.preventDefault();
            document.getElementsByClassName('ant-btn ant-btn-default')[0].click()
        }
        document.getElementsByClassName('container rg-container')[0].addEventListener('click', function() {
            var sas = document.getElementsByClassName('showAnswer');
            for (i = 0; i < sas.length; i++) {
                sas[i].innerText = sas[i].innerText.replace("查看解析", '').replace("解析：无", '').replace("正确答案：", '').replace("解析：否", '').replace("解析：无", '')
            }
        }, false)
        document.getElementsByClassName('cus-with')[0].hidden = true
    }, 3000)
    $(document).keyup(function(event) {
        switch (event.keyCode) {
            case 39:
                document.getElementsByClassName('ant-btn ant-btn-default')[1].click()
                break
            case 37:
                document.getElementsByClassName('ant-btn ant-btn-default')[0].click()
                break
        }
    });
})();
